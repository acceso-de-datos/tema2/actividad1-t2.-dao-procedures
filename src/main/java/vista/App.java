package vista;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import dao.ArticuloDAO;
import dao.ClienteDAO;
import dao.ConexionBD;
import dao.GrupoDAO;
import dao.VendedorDAO;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;
import modelo.Vendedor;

public class App {

	public static void main(String[] args) {

		try {
			ClienteDAO clidao = new ClienteDAO();
			List<Cliente> clientes = clidao.findAll();
			for (Cliente cli : clientes) {
				System.out.println(cli);
			}
			Cliente cli = clidao.findByPK(4);
			if (cli != null) {
				System.out.println("Encontrado cli 4: " + cli);
			}

			// insertar
			Cliente cliInsertar = new Cliente("sergiodao", "direccion dao");
			if (clidao.insert(cliInsertar)) {
				System.out.println("Insertado!");
			}

			// actualizar
			Cliente cliModificar = clidao.findByPK(5);
			if (cliModificar != null) {
				cliModificar.setNombre("otro nombre");
				cliModificar.setDireccion("otra direccion");
				if (clidao.update(cliModificar)) {
					System.out.println("Cliente modificado!!");
				}
			}

			// eliminar
			if (clidao.delete(6)) {
				System.out.println("Cliente borrado!!");
			}
			Cliente cliEliminar = clidao.findByPK(7);
			if (cliEliminar != null) {
				if (clidao.delete(cliEliminar)) {
					System.out.println("Cliente borrado!!");
				}
			}
			
			clidao.cerrar();
			
			
			
			//ARTICULO
			ArticuloDAO artdao = new ArticuloDAO();
			GrupoDAO grupodao= new GrupoDAO();
			Grupo grup = new Grupo();
			
			grup=grupodao.findByPK(1);
			
			List<Articulo> articulos = artdao.findAll();
			for (Articulo art : articulos) {
				System.out.println(art);
			}
			Articulo art = artdao.findByPK(4);
			if (art != null) {
				System.out.println("Encontrado articulo 4: " + art);
			}

			// insertar
			Articulo artInsertar = new Articulo("fabricioDAO", 33,"codDao",grup);
			if (artdao.insert(artInsertar)) {
				System.out.println("Insertado!");
			}
			grup=grupodao.findByPK(3);
			// actualizar
			Articulo artModificar = artdao.findByPK(9);
			if (artModificar != null) {
				artModificar.setNombre("otro nombre");
				artModificar.setPrecio(22);
				artModificar.setCodigo("otro codigo");
				if (artdao.update(artModificar)) {
					System.out.println("Articulo modificado!!");
				}
			}

			// eliminar
			if (artdao.delete(9)) {
				System.out.println("Articulo borrado!!");
			}
			Articulo artEliminar = artdao.findByPK(5);
			if (artEliminar != null) {
				if (artdao.delete(artEliminar)) {
					System.out.println("Articulo borrado!!");
				}
			}
			
			artdao.cerrar();
			
			
			//VENDEDORES
			VendedorDAO ventdao = new VendedorDAO();
			
			grup=grupodao.findByPK(1);
			
			List<Vendedor> vendedores = ventdao.findAll();
			for (Vendedor vent : vendedores) {
				System.out.println(vent);
			}
			Vendedor vent = ventdao.findByPK(4);
			if (vent != null) {
				System.out.println("Encontrado articulo 4: " + vent);
			}

			// insertar
			Vendedor ventInsertar = new Vendedor("fabricioDAO", LocalDate.of(2000, 8, 20),555);
			if (ventdao.insert(ventInsertar)) {
				System.out.println("Insertado!");
			}
			grup=grupodao.findByPK(3);
			// actualizar
			Vendedor ventModificar = ventdao.findByPK(3);
			if (ventModificar != null) {
				ventModificar.setNombre("otro nombre");
				ventModificar.setSalario(22);
				ventModificar.setFecha_ingreso(LocalDate.of(2010, 8, 20));
				if (ventdao.update(ventModificar)) {
					System.out.println("Vendedor modificado!!");
				}
			}

			// eliminar
			if (artdao.delete(3)) {
				System.out.println("Articulo borrado!!");
			}
			Vendedor ventEliminar = ventdao.findByPK(2);
			if (ventEliminar != null) {
				if (ventdao.delete(ventEliminar)) {
					System.out.println("Articulo borrado!!");
				}
			}
			
			ventdao.cerrar();
			
			
			
			//GRUPO
			
			GrupoDAO grupdao= new GrupoDAO();
			
			grup=grupodao.findByPK(1);
			
			List<Grupo> grupos = grupdao.findAll();
			for (Grupo gr : grupos) {
				System.out.println(gr);
			}
			Grupo grp = grupdao.findByPK(4);
			if (grp != null) {
				System.out.println("Encontrado articulo 4: " + grp);
			}

			// insertar
			Grupo grpInsertar = new Grupo("fabricioDAO");
			if (grupdao.insert(grpInsertar)) {
				System.out.println("Insertado!");
			}
			
			// actualizar
			Grupo grpModificar = grupdao.findByPK(9);
			if (grpModificar != null) {
				grpModificar.setDescripcion("otro descripcion");
				if (grupdao.update(grpModificar)) {
					System.out.println("Articulo modificado!!");
				}
			}

			// eliminar
			if (grupdao.delete(9)) {
				System.out.println("Articulo borrado!!");
			}
			Grupo grpEliminar = grupdao.findByPK(5);
			if (grpEliminar != null) {
				if (grupdao.delete(grpEliminar)) {
					System.out.println("Articulo borrado!!");
				}
			}
			
			grupdao.cerrar();
			
			
			ConexionBD.cerrar();

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

}
