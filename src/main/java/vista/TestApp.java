package vista;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

import dao.ArticuloDAO;
import dao.ClienteDAO;
import dao.ConexionBD;
import dao.GrupoDAO;
import dao.VendedorDAO;
import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;
import modelo.Vendedor;

// para corregir la actividad2
public class TestApp {
	final static String TUNOMBRE = "SERGIO";

	public static void main(String[] args) {

		try {
			System.out.println("------------------------------------ CLIENTES ------------------------------------");
			testClienteDAO();
			System.out.println();

			System.out.println("--------------------------------------------------------------------------------");
			System.out.println("------------------------------------ GRUPOS ------------------------------------");
			testGrupoDAO();
			System.out.println();

			System.out.println("-----------------------------------------------------------------------------------");
			System.out.println("------------------------------------ ARTICULOS ------------------------------------");
			testArticuloDAO();

			ConexionBD.cerrar();

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	private static void testArticuloDAO() throws SQLException {
		ArticuloDAO artdao = new ArticuloDAO();

		// insertar
		GrupoDAO gdao = new GrupoDAO();
		Grupo g = gdao.findByPK(1);
		Articulo a = new Articulo("articulo " + TUNOMBRE, 10, "artdao", g);
		if (artdao.insert(a)) {
			System.out.println("Insertado!!");
		}
		if ((a = artdao.insertGenKey(a)) != null) {
			System.out.println("Insertado!! " + a);
		}

		// listar
		List<Articulo> articulos = artdao.findAll();
		for (Articulo art : articulos) {
			System.out.println("\t" + art);
		}

		// obtener por pk
		if ((a = artdao.findByPK(a.getId())) != null) {
			System.out.println("Encontrado articulo: " + a);
		}

		// modificar
		a.setNombre("M - " + a.getNombre());
		if (artdao.update(a)) {
			System.out.println("Modificado!! " + a);
		}

		// listar
		articulos = artdao.findAll();
		for (Articulo art : articulos) {
			System.out.println("\t" + art);
		}

		// borrar
		if (artdao.delete(a)) {
			System.out.println("Borrado!! " + a);
		}
		// listar
		articulos = artdao.findAll();
		for (Articulo art : articulos) {
			System.out.println("\t" + art);
		}

		// buscar por clave ajena
		System.out.println("Lista por grupo...");
		g = gdao.findByPK(2);
		articulos = artdao.findArticulosOfGrupo(g);
		for (Articulo art : articulos) {
			System.out.println("\t" + art);
		}

		artdao.cerrar();
	}

	private static void testGrupoDAO() throws SQLException {
		GrupoDAO grudao = new GrupoDAO();

		// insertar
		Grupo g = new Grupo("grupo " + TUNOMBRE);
		if (grudao.insert(g)) {
			System.out.println("Insertado!!");
		}
		if ((g = grudao.insertGenKey(g)) != null) {
			System.out.println("Insertado!! " + g);
		}

		// listar
		List<Grupo> grupos = grudao.findAll();
		for (Grupo gru : grupos) {
			System.out.println("\t" + gru);
		}

		// obtener por pk
		if ((g = grudao.findByPK(g.getId())) != null) {
			System.out.println("Encontrado grupo: " + g);
		}

		// modificar
		g.setDescripcion("M-" + g.getDescripcion());
		if (grudao.update(g)) {
			System.out.println("Modificado!! " + g);
		}

		// listar
		grupos = grudao.findAll();
		for (Grupo gru : grupos) {
			System.out.println("\t" + gru);
		}

		// borrar
		if (grudao.delete(g)) {
			System.out.println("Borrado!! " + g);
		}

		// listar
		grupos = grudao.findAll();
		for (Grupo gru : grupos) {
			System.out.println("\t" + gru);
		}

		grudao.cerrar();
	}

	private static void testClienteDAO() throws SQLException {
		ClienteDAO clidao = new ClienteDAO();

//		// insertar
		Cliente c = new Cliente("clienteeee " + TUNOMBRE, "direccion...");
		if (clidao.insert(c)) {
			System.out.println("Insertado!!");
		}
		if ((c = clidao.insertGenKey(c)) != null) {
			System.out.println("Insertado!! " + c);
		}
//
//		// listar
		List<Cliente> clientes = clidao.findAll();
		for (Cliente cli : clientes) {
			System.out.println("\t" + cli);
		}

//		// obtener por pk
		if ((c = clidao.findByPK(c.getId())) != null) {
			System.out.println("Encontrado cliente: " + c);
		}

		// modificar
		c.setNombre("M - " + c.getNombre());
		if (clidao.update(c)) {
			System.out.println("Modificado!! " + c);
		}

//		// listar
		clientes = clidao.findAll();
		for (Cliente cli : clientes) {
			System.out.println("\t" + cli);
		}

//		// borrar
		if (clidao.delete(c)) {
			System.out.println("Borrado!! " + c);
		}
//		// listar
		clientes = clidao.findAll();
		for (Cliente cli : clientes) {
			System.out.println("\t" + cli);
		}

		clidao.cerrar();
	}

}
