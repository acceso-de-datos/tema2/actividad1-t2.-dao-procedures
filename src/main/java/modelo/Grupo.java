package modelo;

public class Grupo {
private int id;
private String descripcion;
	public Grupo() {
		// TODO Auto-generated constructor stub
	}
	public Grupo(String descripcion) {
	
	
		this.descripcion = descripcion;
	}
	public Grupo(int id ,String descripcion) {
		this.id=id;
		this.descripcion = descripcion;
	}

	public int getId() {
		
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "Grupo{" +
				"id=" + id +
				", descripcion='" + descripcion + '\'' +
				'}';
	}
}
