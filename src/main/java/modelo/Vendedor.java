package modelo;


import java.time.LocalDate;

public class Vendedor {
	private int id;
	private String nombre;
	private LocalDate fecha_ingreso;
	private float salario;
	
	public Vendedor(String nombre, LocalDate fecha_ingreso, float salario) {
		
		this.nombre = nombre;
		this.fecha_ingreso = fecha_ingreso;
		this.salario = salario;
	}

	public Vendedor(int id, String nombre, LocalDate fecha_ingreso, float salario) {

		this.id = id;
		this.nombre = nombre;
		this.fecha_ingreso = fecha_ingreso;
		this.salario = salario;
	}
	public Vendedor( String nombre, LocalDate fecha_ingreso) {

		this.nombre = nombre;
		this.fecha_ingreso = fecha_ingreso;

	}

	public Vendedor() {
		// TODO Auto-generated constructor stub
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFecha_ingreso() {
		return fecha_ingreso;
	}

	public void setFecha_ingreso(LocalDate fecha_ingreso) {
		this.fecha_ingreso = fecha_ingreso;
	}

	public float getSalario() {
		return salario;
	}

	public void setSalario(float salario) {
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "Vendedor [id=" + id + ", nombre=" + nombre + ", fecha_ingreso=" + fecha_ingreso + ", salario=" + salario
				+ "]";
	}

}
