package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modelo.Articulo;
import modelo.Cliente;
import modelo.Grupo;
import modelo.Vendedor;

public class ArticuloDAO implements GenericDAO<Articulo> {

    final String SQLSELECTALL = "{ CALL empresa_ad.findall_articulo() }";
    final String SQLSELECTPK = "{ CALL empresa_ad.findbypk_articulo(?) }";
    final String SQLINSERT = "{ CALL empresa_ad.insert_articulo(?,?,?,?,?) }";
    final String SQLFINDBYEXAMPLE = "SELECT * FROM articulos WHERE nombre like ? AND  precio like ? AND  codigo like ? AND grupo like ?";
    final String SQLUPDATE = "{ CALL empresa_ad.update_articulo(?,?,?,?,?) }";
    final String SQLDELETE = "{ CALL empresa_ad.delete_articulo(?) }";
    private final CallableStatement pstSelectPK;
    private final CallableStatement pstSelectAll;
    private PreparedStatement pstSelectByExample;
    private final CallableStatement pstInsert;
    private final CallableStatement pstInsertGenKey;
    private final CallableStatement pstUpdate;
    private final CallableStatement pstDelete;

    public ArticuloDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareCall(SQLSELECTPK);
        pstSelectAll = con.prepareCall(SQLSELECTALL);
        pstSelectByExample = con.prepareStatement(SQLFINDBYEXAMPLE);
        pstInsert = con.prepareCall(SQLINSERT);
        pstInsertGenKey = con.prepareCall(SQLINSERT);
        pstUpdate = con.prepareCall(SQLUPDATE);
        pstDelete = con.prepareCall(SQLDELETE);
    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    @Override
    public Articulo findByPK(int id) throws SQLException {
        Articulo a = null;
        GrupoDAO grDAO = new GrupoDAO();
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.first()) {
            return new Articulo(id, rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo")));
        }
        rs.close();
        return a;
    }

    @Override
    public List<Articulo> findAll() throws SQLException {
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        ResultSet rs = pstSelectAll.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }

    public List<Articulo> findArticulosOfGrupo(Grupo grup) throws SQLException {
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();
        this.pstSelectByExample.setString(1, "%");
        this.pstSelectByExample.setString(2, "%");
        this.pstSelectByExample.setString(3, "%");
        this.pstSelectByExample.setString(4, "%" + grup.getId() + "%");
        ResultSet rs = pstSelectByExample.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }

    @Override
    public List<Articulo> findByExample(Articulo arti) throws SQLException {
        // nombre like '%?%' AND  precio like '%?%' AND  codigo like '%?%' AND grupo like '%?%'
        GrupoDAO grDAO = new GrupoDAO();
        List<Articulo> listaArticulos = new ArrayList<Articulo>();


        if (arti.getNombre() != null) {

            this.pstSelectByExample.setString(1, "%" + arti.getNombre() + "%");

        } else {
            this.pstSelectByExample.setString(1, "%");
        }
        if (arti.getPrecio() > 0) {
            this.pstSelectByExample.setFloat(2, arti.getPrecio());

        } else {
            this.pstSelectByExample.setString(2, "%");

        }
        if (arti.getCodigo() != null) {
            this.pstSelectByExample.setString(3, arti.getCodigo());

        } else {
            this.pstSelectByExample.setString(3, "%");

        }
        if (arti.getGrupo() != null) {
            this.pstSelectByExample.setString(4, "%" + arti.getGrupo().getId() + "%");
        } else {
            this.pstSelectByExample.setString(4, "%");

        }


        ResultSet rs = pstSelectByExample.executeQuery();
        while (rs.next()) {
            listaArticulos.add(new Articulo(rs.getInt("id"), rs.getString("nombre"), rs.getFloat("precio"), rs.getString("codigo"), grDAO.findByPK(rs.getInt("grupo"))));
        }
        rs.close();
        return listaArticulos;
    }


    @Override
    public boolean insert(Articulo arti) throws SQLException {
        pstInsert.setString(1, arti.getNombre());
        pstInsert.setFloat(2, arti.getPrecio());
        pstInsert.setString(3, arti.getCodigo());
        pstInsert.setInt(4, arti.getGrupo().getId());
        pstInsert.registerOutParameter(5, Types.INTEGER);
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Articulo insertGenKey(Articulo arti) throws SQLException {
        Articulo artic = null;
        ;
        pstInsertGenKey.setString(1, arti.getNombre());
        pstInsertGenKey.setFloat(2, arti.getPrecio());
        pstInsertGenKey.setString(3, arti.getCodigo());
        pstInsertGenKey.setInt(4, arti.getGrupo().getId());
        pstInsertGenKey.registerOutParameter(5, Types.INTEGER);
        int insertados = pstInsertGenKey.executeUpdate();
        if (insertados == 1) {


            artic = new Articulo(pstInsertGenKey.getInt(5), arti.getNombre(), arti.getPrecio(), arti.getCodigo(), arti.getGrupo());
            return artic;
        }
        return artic;
    }

    @Override
    public boolean update(Articulo arti) throws SQLException {
        pstUpdate.setString(1, arti.getNombre());
        pstUpdate.setFloat(2, arti.getPrecio());
        pstUpdate.setString(3, arti.getCodigo());
        pstUpdate.setInt(4, arti.getGrupo().getId());
        pstUpdate.setInt(5, arti.getId());
        int actualizados = pstUpdate.executeUpdate();
        return (actualizados == 1);
    }

    @Override
    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    @Override
    public boolean delete(Articulo arti) throws SQLException {
        return this.delete(arti.getId());
    }

}
