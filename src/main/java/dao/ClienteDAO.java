package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modelo.Cliente;

/**
 * @author sergio
 */
public class ClienteDAO implements GenericDAO<Cliente> {

    final String SQLSELECTALL = "{ CALL findall_cliente() }";
    final String SQLFINDBYEXAMPLE = "SELECT * FROM clientes WHERE nombre like ? and direccion like ?";

    final String SQLSELECTPK = "{ CALL empresa_ad.findbypk_cliente(?) }";
    final String SQLINSERT = "{ CALL empresa_ad.insert_cliente(?,?,?) }";
    final String SQLUPDATE = "{ CALL empresa_ad.update_cliente(?,?,?) }";
    final String SQLDELETE = "{ CALL empresa_ad.delete_cliente(?) }";
    private final CallableStatement pstSelectPK;
    private final CallableStatement pstSelectAll;
    private final PreparedStatement pstSelectByExample;

    private final CallableStatement pstInsert;
    private final CallableStatement pstInsertGenKey;
    private final CallableStatement pstUpdate;
    private final CallableStatement pstDelete;

    public ClienteDAO() throws SQLException {
        Connection con = ConexionBD.getConexion();
        pstSelectPK = con.prepareCall(SQLSELECTPK);
        pstSelectAll = con.prepareCall(SQLSELECTALL);
        pstSelectByExample = con.prepareStatement(SQLFINDBYEXAMPLE);
        pstInsert = con.prepareCall(SQLINSERT);
        pstInsertGenKey = con.prepareCall(SQLINSERT);
        pstUpdate = con.prepareCall(SQLUPDATE);
        pstDelete = con.prepareCall(SQLDELETE);

    }

    public void cerrar() throws SQLException {
        pstSelectPK.close();
        pstSelectAll.close();
        pstSelectByExample.close();
        pstInsert.close();
        pstUpdate.close();
        pstDelete.close();
    }

    public Cliente findByPK(int id) throws SQLException {
        Cliente c = null;
        pstSelectPK.setInt(1, id);
        ResultSet rs = pstSelectPK.executeQuery();
        if (rs.first()) {
            return new Cliente(id, rs.getString("nombre"), rs.getString("direccion"));
        }
        rs.close();
        return c;
    }

    public List<Cliente> findAll() throws SQLException {
        List<Cliente> listaCliente = new ArrayList<Cliente>();
        pstSelectAll.execute();
        ResultSet rs = pstSelectAll.getResultSet();

        while (rs.next()) {
            listaCliente.add(new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
        }

        return listaCliente;
    }

    @Override
    public List<Cliente> findByExample(Cliente cli) throws SQLException {


        List<Cliente> listaCliente = new ArrayList<Cliente>();

        if (cli.getNombre() != null) {

            this.pstSelectByExample.setString(1, "%" + cli.getNombre() + "%");

        } else {
            this.pstSelectByExample.setString(1, "%");
        }
        if (cli.getDireccion() != null) {
            this.pstSelectByExample.setString(2, "%" + cli.getDireccion() + "%");

        } else {
            this.pstSelectByExample.setString(2, "%");
        }
        ;

        ResultSet rs = pstSelectByExample.executeQuery();
        if (rs.first()) {
            listaCliente.add(new Cliente(rs.getInt("id"), rs.getString("nombre"), rs.getString("direccion")));
        }

        return listaCliente;
    }

    public boolean insert(Cliente cliInsertar) throws SQLException {
        pstInsert.setString(1, cliInsertar.getNombre());
        pstInsert.setString(2, cliInsertar.getDireccion());
        pstInsert.registerOutParameter(3, Types.INTEGER);
        int insertados = pstInsert.executeUpdate();
        //devuelve
        return (insertados == 1);
    }

    @Override
    public Cliente insertGenKey(Cliente cliInsertar) throws SQLException {
        Cliente cli = null;
        pstInsertGenKey.setString(1, cliInsertar.getNombre());
        pstInsertGenKey.setString(2, cliInsertar.getDireccion());
        pstInsertGenKey.registerOutParameter(3, Types.INTEGER);
        int insertados = pstInsertGenKey.executeUpdate();
        if (insertados == 1) {

            cli = new Cliente(pstInsertGenKey.getInt(3), cliInsertar.getNombre(), cliInsertar.getDireccion());
            return cli;
        }
        return cli;
    }

    public boolean update(Cliente cliActualizar) throws SQLException {
        pstUpdate.setString(1, cliActualizar.getNombre());
        pstUpdate.setString(2, cliActualizar.getDireccion());
        pstUpdate.setInt(3, cliActualizar.getId());
        int actualizados = pstUpdate.executeUpdate();

        return (actualizados == 1);
    }

    public boolean delete(int id) throws SQLException {
        pstDelete.setInt(1, id);
        int borrados = pstDelete.executeUpdate();
        return (borrados == 1);
    }

    public boolean delete(Cliente cliEliminar) throws SQLException {
        return this.delete(cliEliminar.getId());
    }

}
